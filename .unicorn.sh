
#!/bin/bash
# This file is meant to be executed via systemd.
source /usr/local/rvm/scripts/rvm
source /etc/profile.d/rvm.sh
export ruby_ver=$(rvm list default string)

export CONFIGURED=yes
export TIMEOUT=50
export APP_ROOT=/home/rails/gprs
export RAILS_ENV="production"
export GEM_HOME="/home/rails/gprs/vendor/bundle"
export GEM_PATH="/home/rails/gprs/vendor/bundle:/usr/local/rvm/gems/${ruby_ver}:/usr/local/rvm/gems/${ruby_ver}@global"
export PATH="/home/rails/gprs/vendor/bundle/bin:/usr/local/rvm/gems/${ruby_ver}/bin:${PATH}"

# Passwords
export SECRET_KEY_BASE=bc1a446659bdfeae249f559a2c4aaeda8d4e1d586214cc222a92b2aa158723570310a4b2eb12373e1ed64dae3d8a23a8982f26a59772bc2d75fb60d79a6859b9
export APP_DATABASE_PASSWORD=c93ed394ec95f0a75b9f1837978a3cdf

# Execute the unicorn process
/home/rails/gprs/vendor/bundle/bin/unicorn \
        -c /etc/unicorn.conf -E production --debug
