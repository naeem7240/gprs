class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.string :client_id
      t.string :topic
      t.text :message

      t.timestamps
    end
  end
end
