class Tracker < ApplicationRecord
	def self.search(topic, page)
	  if topic
	    where('name LIKE ?', "%#{topic}%").paginate(page: page, per_page: 30).order('id DESC')
	  else
	    paginate(page: page, per_page: 30).order('id DESC') 
	  end
	end
end
