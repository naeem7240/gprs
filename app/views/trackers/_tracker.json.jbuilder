json.extract! tracker, :id, :client_id, :topic, :message, :created_at, :updated_at
json.url tracker_url(tracker, format: :json)
